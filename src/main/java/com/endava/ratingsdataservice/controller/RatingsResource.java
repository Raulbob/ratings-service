package com.endava.ratingsdataservice.controller;

import com.endava.ratingsdataservice.model.Rating;
import com.endava.ratingsdataservice.model.UserRating;
import com.endava.ratingsdataservice.service.RatingsService;
import com.endava.ratingsdataservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("ratingsdata")
public class RatingsResource {

    @Autowired
    private UserService userService;

    @Autowired
    private RatingsService ratingsService;

    @RequestMapping("/{movieId}")
    public Rating getRatings(@PathVariable("movieId") long movieId) {
        return new Rating(movieId, 4);
    }

    @RequestMapping("users/{userId}")
    public UserRating getUserRatings(@PathVariable("userId") long userId) {
        if (!userService.existsUser(userId)) {
            throw new IllegalArgumentException("User not found!");
        }
        return new UserRating(ratingsService.findRatingsForUserId(userId));
    }

}
