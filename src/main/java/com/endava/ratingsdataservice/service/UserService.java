package com.endava.ratingsdataservice.service;

import com.endava.ratingsdataservice.model.User;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private List<User> userList = Arrays.asList(
            new User(1, "admin", "admin"),
            new User(2, "user", "user"),
            new User(3, "foo", "foo"));


    public boolean existsUser(long userId) {
        return userList.stream().map(user -> user.getId()).collect(Collectors.toList()).contains(userId);
    }

    public List<User> findAllUsers() {
        return userList;
    }

}
