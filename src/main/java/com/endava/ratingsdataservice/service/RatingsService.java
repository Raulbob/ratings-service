package com.endava.ratingsdataservice.service;

import com.endava.ratingsdataservice.model.Rating;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RatingsService {

    //map of UserId and List of ratings.
    private Map<Long, List<Rating>> ratings = new HashMap<Long, List<Rating>>() {{
        put(1l, Arrays.asList(
                new Rating(1, 4),
                new Rating(2, 5)));
        put(2l, Arrays.asList(new Rating(1, 4)));
    }};
    
    public List<Rating> findRatingsForUserId(long userId) {
        if (ratings.containsKey(userId)) {
            return ratings.get(userId);
        } else {
            return new ArrayList<>();
        }
    }

}
